﻿Shader "Maikel/FX/UnlitCutoutMasked"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
        _ID("Mask ID", Int) = 1
        _CutoutThreshold ("Cutout Threshold", Range(0,1)) = 0
        _Color ("Tint", Color) = (1,1,1,1)
        _ColorMask ("Color Mask", Float) = 15


	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue" = "Geometry+3" }
		LOD 100
		ZTest Greater
        Stencil {
            Ref [_ID]
            Comp equal
        }
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _CutoutThreshold;
			float4  _Color;
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);

				clip(col.a - _CutoutThreshold);
				return col*i.color * _Color;
			}
			ENDCG
		}
	}
}
