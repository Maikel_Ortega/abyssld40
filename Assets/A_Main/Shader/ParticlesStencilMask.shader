﻿Shader "Maikel/FX/ParticlesCutoutStencilMask" {
    Properties{
 		_MainTex ("Texture", 2D) = "white" {}
        _ID("Mask ID", Int) = 1
		_CutoutThreshold ("Cutout Threshold", Range(0,1)) = 0

    }
        SubShader{
            Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+1" }
            ColorMask 0
            ZWrite on

            Stencil
            {
                Ref[_ID]
                Comp always
                Pass replace
            }

        Pass{
        			CGPROGRAM

            #pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

        struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
			};

   		sampler2D _MainTex;
   		float _CutoutThreshold;
		float4 _MainTex_ST;

        v2f vert(appdata v) 
        {
            v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = TRANSFORM_TEX(v.uv, _MainTex);
			o.color = v.color;
			return o;
        }

        half4 frag(v2f i) : SV_Target
        {
			fixed4 col = tex2D(_MainTex, i.uv);
			clip(col.a -_CutoutThreshold);
			col = col* i.color;
			return col;        
		}
            ENDCG
 
    }
}
}