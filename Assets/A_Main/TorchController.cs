﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class TorchConfiguration
{
	public float bsize;
	public float isize;
	public float blifetime;
	public float ilifetime;
	public float lightIntensity;
}

public class TorchController : MonoBehaviour 
{
	public ParticleSystem mBurstFlames;
	public Light torchLight;	
	public ParticleSystem mBlueFlame;
	public ParticleSystem mInsightFlame;
	public ParticleSystem fireBreath;
	public GameObject firebreathCollider;
	public List<TorchConfiguration> torchConfigs;
	public int torchIndex =0;
	public float currentTimer = 0;
	public float fireDuration = 30f;
	public float breathConsumption = 2f;
	public float minTorchCutout = 0.45f;
	public float maxTorchCutout = 0.7f;
	public MeshRenderer torchRenderer;
	public bool dry = false;
	public AudioSource fireAs;
	public AudioSource fireBreathAs;
	public AudioSource fireIn;
	public AudioSource fireOut;
	public AudioSource musicSource;

	Material mat;



	public IEnumerator FadeInMusic(){
		while(musicSource.volume<1.0f){
			musicSource.volume +=0.1f *Time.deltaTime / 2f; //2f es el tiempo que tarda en hacer el fade
			yield return  null;
		}

	}

	public IEnumerator FadeInTorchBreath(){
		while(fireBreathAs.volume<1.0f){
			fireBreathAs.volume +=0.1f *Time.deltaTime / 0.2f; //2f es el tiempo que tarda en hacer el fade
			yield return  null;
		}

	}

	public IEnumerator FadeOutTorchBreath(){
		while(fireBreathAs.volume>0.0f  ){
			fireBreathAs.volume -=0.1f *Time.deltaTime / 0.2f; //2f es el tiempo que tarda en hacer el fade
			yield return  null;
		}
		fireAs.Stop ();
	}

	void Awake()
	{
		mat = torchRenderer.material;
		StartCoroutine (FadeInMusic());
	}

	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.O)) 
		{
			PreviousTorchConfig ();
		}
		if (Input.GetKeyDown (KeyCode.P)) 
		{
			NextTorchConfig ();
		}
		if (Input.GetKey (KeyCode.LeftShift)) 
		{
			currentTimer -= breathConsumption * Time.deltaTime;
		}
		if (Input.GetMouseButton (0)) {
			FireBreath (true);
		} 

		else if(Input.GetMouseButtonUp(0))
		{
			FireBreath(false);

		}

		CheckTimers ();
	}

	void FireBreath(bool activate)
	{
		if (!dry && activate) {
			//Audio Control fireBreath
			if (!fireBreathAs.isPlaying) {
				fireAs.pitch = 1.5f;
				fireAs.volume = 0.3f;
				fireBreathAs.Play ();
			}

			fireBreath.Play ();
			mBlueFlame.Stop ();
			mInsightFlame.Stop ();
			currentTimer -= Time.deltaTime * breathConsumption;
			firebreathCollider.SetActive (true);
		}
		else 
		{
			//return Audio to normal pitch
			fireAs.pitch = 1.0f;
			fireAs.volume = 1.0f;
			fireBreathAs.Stop ();

			firebreathCollider.SetActive (false);
			fireBreath.Stop ();
			if (!dry) {
				mBlueFlame.Play ();
				mInsightFlame.Play ();
			} 
		}
	}

	void CheckTimers()
	{
		if (dry) {
			
			return;
		}
		currentTimer -= Time.deltaTime;
		mat.SetFloat ("_CutoutThreshold", (1-(currentTimer / fireDuration)) * (maxTorchCutout - minTorchCutout) + minTorchCutout);
		if (currentTimer < 0) 
		{
			PreviousTorchConfig ();
			currentTimer = fireDuration;

		}
	}

	void NextTorchConfig()
	{
		torchIndex = (torchIndex + 1)% torchConfigs.Count;
		currentTimer = fireDuration;
		ChangeTorchConfig (torchIndex);
	}

	void PreviousTorchConfig()
	{
		if (torchIndex == 0) 
		{
			TurnOutTorch ();
		} else 
		{
			torchIndex = ((torchIndex - 1)+torchConfigs.Count ) % torchConfigs.Count;
			ChangeTorchConfig (torchIndex);
		}
	}
	void TurnOutTorch()
	{
		mBurstFlames.Play ();

		torchLight.intensity = 0;
		mBlueFlame.Stop ();
		dry = true;
		mInsightFlame.Stop ();
		//Stop audio 

		fireOut.Play ();



	}

	void ChangeTorchConfig(int index)
	{
		if (dry) {
			mBlueFlame.Play ();
			mInsightFlame.Play ();
			dry = false;
		}
		mBurstFlames.Play ();

		if (index < torchConfigs.Count) 
		{
			SetTorchConfig (torchConfigs [index]);
		}
	}
	void SetTorchConfig(TorchConfiguration c)
	{
		var bmain = mBlueFlame.main;
		var imain = mInsightFlame.main;
		if (!fireAs.isPlaying) {
			fireAs.Play ();
		}
		bmain.startSizeMultiplier = c.bsize;
		imain.startSizeMultiplier = c.isize;
		bmain.startLifetimeMultiplier = c.blifetime;
		imain.startLifetimeMultiplier = c.ilifetime;
		torchLight.intensity = c.lightIntensity;
	}

	void OnTriggerEnter(Collider col)
	{
		
		if (col.tag == "Lighter") 
		{	
			if(!fireIn.isPlaying)
				fireIn.Play ();
			torchIndex = 1;
			NextTorchConfig ();

		}
	}
}
