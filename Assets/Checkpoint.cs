﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public int id = 1;

    public const string CHECK_KEY = "CHECKPOINT";


    private void Awake()
    {
        if (PlayerPrefs.GetInt(CHECK_KEY) > id)
        {
            this.GetComponent<BoxCollider>().enabled = false;
        }
    }

    void SaveCheckpoint()
    {
        PlayerPrefs.SetInt(CHECK_KEY, id);

    }

    public void Spawn(PlayerController p)
    {
        p.transform.position = this.transform.position;
        p.transform.rotation = this.transform.rotation;
        p.ActivateTorch();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            SaveCheckpoint();
            this.GetComponent<BoxCollider>().enabled = false;
        }
    }
}
