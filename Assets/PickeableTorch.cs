﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickeableTorch : MonoBehaviour {

    public AudioSource musicSource;

	 public IEnumerator FadeInMusic(){
		while(musicSource.volume<1.0f){
			musicSource.volume +=0.1f *Time.deltaTime / 2f; //2f es el tiempo que tarda en hacer el fade
			yield return  null;
		}

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Player") 
		{
			if (Vector3.Dot (col.transform.forward, transform.forward) < 0) {
				//Debug.Log ("LESS");
			} else 
			{
				//Debug.Log ("MORE");
                musicSource.Play();
				StartCoroutine (FadeInMusic());
				col.transform.GetChild (0).GetChild (0).gameObject.SetActive (true);
				this.gameObject.SetActive (false);
			}
		}
	}
}
