﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.PostProcessing;
public class PlayerController : MonoBehaviour
{
    public UIManager uimanager;
    public FirstPersonController fpController;
    public TorchController mTorch;
    public AnimationCurve effectAnimationCurve;
    public AudioSource deathAS;

    private void Awake()
    {
        InitGame();
    }

    void InitGame()
    {
        List<Checkpoint> l = new List<Checkpoint>( GameObject.FindObjectsOfType<Checkpoint>());
        Checkpoint c = l.Find(x => x.id == PlayerPrefs.GetInt(Checkpoint.CHECK_KEY));
        if (c != null)
        {
            c.Spawn(this);
        }

    }

    public void ActivateTorch()
    {
        this.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
    }

    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            Death();
        }

        UpdateUI();
	}

    void UpdateUI()
    {
        if (mTorch.dry || !mTorch.gameObject.activeSelf)
        {
            uimanager.SetTorchLevel(0, 0);
        }
        else
        {
            uimanager.SetTorchLevel(mTorch.torchIndex, mTorch.currentTimer / mTorch.fireDuration);
        }
    }

    public void Death()
    {
        fpController.m_MouseLook.SetCursorLock(false);
        fpController.enabled = false;
        deathAS.Play();
        uimanager.SetGameOverImage();
    }

    public void OnDwellerSpawn(DwellerSpawner spwn)
    {
        


        float seconds = 2.0f;
        this.fpController.m_MouseLook.XSensitivity = 0.1f;
        this.fpController.m_MouseLook.YSensitivity = 0.1f;
        StartCoroutine(FovChangeCoroutine(seconds,effectAnimationCurve,45,SecondStage));
        StartCoroutine(TimeScaleLerp(2f, effectAnimationCurve, 0.5f));
        StartCoroutine(BloomLerp(2f, effectAnimationCurve, 20f));

    }

    void SecondStage()
    {
        StartCoroutine(FovChangeCoroutine(1.0f, effectAnimationCurve, 60, OnEffectEnded));
        StartCoroutine(TimeScaleLerp(0.25f, effectAnimationCurve, 1f));
        StartCoroutine(BloomLerp(1f, effectAnimationCurve, 4f));

    }

    void OnEffectEnded()
    {
        this.fpController.m_MouseLook.XSensitivity = 1f;
        this.fpController.m_MouseLook.YSensitivity = 1f;
    }

    IEnumerator BloomLerp(float seconds, AnimationCurve curve, float newValue, System.Action callback = null)
    {
        PostProcessingBehaviour ppp = Camera.main.GetComponent<PostProcessingBehaviour>();
        BloomModel.Settings bloomSettings = ppp.profile.bloom.settings;

        float origin = bloomSettings.bloom.intensity;
        float totalSeconds = seconds;      

        while (seconds > 0)
        {
            seconds -= Time.deltaTime;
            float normalizedValue = 1 - seconds / totalSeconds;            
            bloomSettings.bloom.intensity = Mathf.Lerp(origin, newValue, curve.Evaluate(normalizedValue));
            ppp.profile.bloom.settings = bloomSettings;
            yield return null;
        }
        if (callback != null)
        {
            callback.Invoke();
        }
    }

    IEnumerator TimeScaleLerp(float seconds, AnimationCurve curve, float newValue, System.Action callback = null)
    {
        float origin = Time.timeScale;
        float totalSeconds = seconds;

        while (seconds > 0)
        {
            seconds -= Time.deltaTime;

            float normalizedValue = 1 - seconds / totalSeconds;
            Time.timeScale = Mathf.Lerp(origin, newValue, curve.Evaluate(normalizedValue));
            yield return null;
        }
        if (callback != null)
        {
            callback.Invoke();
        }
    }

    IEnumerator FovChangeCoroutine(float seconds, AnimationCurve curve, float newFOV, System.Action callback= null)
    {
        float normalFOV = Camera.main.fieldOfView;
        float totalSeconds = seconds;

        while (seconds > 0)
        {
            seconds -= Time.deltaTime;

            float normalizedValue = 1 - seconds / totalSeconds;

            Camera.main.fieldOfView = Mathf.Lerp(normalFOV, newFOV, curve.Evaluate(normalizedValue));
            yield return null;
        }
        if (callback != null)
        {
            callback.Invoke();
        }
    }

    public bool CanSeeDweller(Dweller d)
    {
        return false;
    }
}
