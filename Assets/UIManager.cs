﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public Image gameoverImg;
    public Image retryButton;
    public List<Image> torchIcons;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.U))
        {
            SetGameOverImage();
        }
	}

    public void SetGameOverImage()
    {
        StartCoroutine(ChangeImageAlpha(gameoverImg, 1, 2f,ShowRetryButton));
    }

    IEnumerator ChangeImageAlpha(Image img, float target, float seconds, System.Action callback=null)
    {
        Color c = img.color;
        float origin = c.a;
        float counter = 0;
        while (counter < seconds)
        {
            c.a = Mathf.Lerp(origin, target, counter / seconds);
            img.color = c;
            counter += Time.deltaTime;
            yield return null;
        }
        if (callback != null)
        {
            callback.Invoke();
        }
    }

    public void SetTorchLevel(int level, float normalizedValue)
    {
        for (int i = 0; i < torchIcons.Count; i++)
        {
            if (i <= level)
            {
                torchIcons[i].enabled = true;
                torchIcons[i].fillAmount = 1;
            }
            else
            {
                torchIcons[i].enabled = false;
                torchIcons[i].fillAmount = 0;
            }
        }
        torchIcons[level].fillAmount = normalizedValue;
    }

    void ShowRetryButton()
    {
        StartCoroutine(ChangeImageAlpha(retryButton, 1, 1, ShowRetryText));
    }

    void ShowRetryText()
    {
        retryButton.transform.GetChild(0).gameObject.SetActive(true);
    }

    public void OnClickRetry()
    {
        SceneManager.LoadScene("Gameplay");
    }
}
