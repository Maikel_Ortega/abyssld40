﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightableFire : MonoBehaviour 
{
	public GameObject lightedFire;
	public bool lit = false;

	void Awake()
	{
		if (lit) 
		{
			LightFire ();
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.tag == "Firebreath") 
		{
			LightFire ();
		}
	}

	void LightFire()
	{
		lightedFire.SetActive (true);
	}
}
