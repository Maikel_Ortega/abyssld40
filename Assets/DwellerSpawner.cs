﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DwellerSpawner : MonoBehaviour
{
    PlayerController player;
    public float maxDistance = 10f;
    public bool active = true;
    public GameObject dwellerPrefab;


    private void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
    }

    void Update ()
    {
        if (active)
        { 
            CheckRay();
        }
    }

    void CheckRay()
    {

        Vector3 p = Camera.main.WorldToScreenPoint(transform.position);

        Ray r = Camera.main.ScreenPointToRay(p);
        //Ray r = new Ray( transform.position, Camera.main.transform.position);
        RaycastHit info;
        if (Physics.Raycast(r,out info,  maxDistance))
        {
            if (info.collider.tag == "Solid")
            {
                return;
            }
        }

        RaycastHit[] hits = Physics.RaycastAll(r, maxDistance);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.tag == "TorchSightCollider")
            {
                Debug.Log("I SEE YOU!");
                Debug.DrawLine(transform.position, Camera.main.transform.position, Color.green);
                player.OnDwellerSpawn(this);
                GameObject g = Instantiate(dwellerPrefab, transform.position, transform.rotation) as GameObject;
                g.GetComponent<Dweller>().Spawned();
                active = false;
            }
        }
    }
}
