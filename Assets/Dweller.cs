﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dweller : MonoBehaviour
{
    public PlayerController player;
    public float revealRadius = 10f;
    public bool revealed = false;

    public GameObject revealedBody;
    public GameObject hiddenBody;

    public AutonomousAgent mAgent;
    public Seek mSeekBehavior;
    public bool chasing = false;

    public  enum DWELLER_STATES { SPAWNING, CHASING, PATROL}
    public DWELLER_STATES state;


    void Awake()
    {
        player = GameObject.FindObjectOfType<PlayerController>();
        mAgent = this.gameObject.GetComponent<AutonomousAgent>();
        mSeekBehavior = this.gameObject.GetComponent<Seek>();

    }

    public void Spawned()
    {
        state = DWELLER_STATES.SPAWNING;
        mAgent.enabled = false;
        RevealBody(false);
    }

    void Update()
    {
        CheckSight();

        if (state == DWELLER_STATES.CHASING)
        {
            CheckDistance();

            mSeekBehavior.target = player.transform.position;
            CheckFireLevel();
        }
        else if (state == DWELLER_STATES.SPAWNING)
        {
            transform.rotation.SetLookRotation(player.transform.position - transform.position);            
        }

    }

    public void ChangeToChaseState()
    {
        mAgent.enabled = true;
        state = DWELLER_STATES.CHASING;
        mAgent.behaviorConfigsByPriority.Find(x => x.behavior == mSeekBehavior).weight *= 2;

    }

    void CheckFireLevel()
    {
        if (player.mTorch.dry)
        {
            state = DWELLER_STATES.PATROL;
        }
    }

    void CheckSight()
    {
        if(player.CanSeeDweller(this))
        {

        }
    }

    void RevealBody(bool doreveal)
    {
        revealed = doreveal;
        revealedBody.SetActive(doreveal);
        hiddenBody.SetActive(!doreveal);
    }

    void CheckDistance()
    {
        if (!revealed && Vector3.Distance(transform.position, player.transform.position) < revealRadius)
        {
            RevealBody(true);
        }

        if (revealed && Vector3.Distance(transform.position, player.transform.position) > revealRadius*1.5f)
        {
            RevealBody(false);
        }
    }    

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            player.Death();
        }
    }
}
