﻿Shader "Maikel/FX/CutoutMaskParticlesMasked"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_TintColor ("Main Color", color) = (1,1,1,1)
		_DarkSide ("Inner shade factor", Range(0,1)) = 0.6
		_CutoutMask("Cutout Mask", 2D) = "white" {}
		_CutoutThreshold ("Cutout Threshold", Range(0,1)) = 0
		 _ID("Mask ID", Int) = 1

	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "Queue" = "Geometry+3"}
		LOD 100
		ZTest Greater
        Stencil {
            Ref [_ID]
            Comp equal
        }
		Pass
		{
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvmask : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
			};

			sampler2D _MainTex;
			sampler2D _CutoutMask;
			float4 _MainTex_ST;
			float4 _CutoutMask_ST;
			float _CutoutThreshold;
			float _DarkSide;
			float4 _TintColor;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uvmask = TRANSFORM_TEX(v.uv, _CutoutMask);
				o.color = v.color;

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 cut = tex2D(_CutoutMask, i.uvmask);
				clip(i.color.a - cut.r);
				col = col *i.color;
				return col*_DarkSide;		
			}
			ENDCG
		}

		Pass
		{
			Cull Back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
							// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvmask : TEXCOORD1;
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
			};

			sampler2D _MainTex;
			sampler2D _CutoutMask;
			float4 _MainTex_ST;
			float4 _CutoutMask_ST;
			float _CutoutThreshold;
			float4 _TintColor;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uvmask = TRANSFORM_TEX(v.uv, _CutoutMask);
				o.color = v.color;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 cut = tex2D(_CutoutMask, i.uvmask);
				clip(i.color.a - cut.r);
				col = col *i.color;
				return col;
			}
			ENDCG
		}
	}
}
