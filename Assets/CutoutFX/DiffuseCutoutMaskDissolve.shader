﻿Shader "Maikel/FX/DiffuseCutoutMask"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_TintColor ("Main Color", color) = (1,1,1,1)
		_CutoutMask("Cutout Mask", 2D) = "white" {}
		_CutoutThreshold ("Cutout Threshold", Range(0,1)) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Pass
		{
			Cull Front

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvmask : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
			};

			sampler2D _MainTex;
			sampler2D _CutoutMask;
			float4 _MainTex_ST;
			float4 _CutoutMask_ST;
			float _CutoutThreshold;
			float4 _TintColor;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);				
				o.uvmask = TRANSFORM_TEX(v.uv, _CutoutMask);
				o.color = v.color;

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 cut = tex2D(_CutoutMask, i.uvmask);
				clip(i.color.a - cut.r);
				col = col *i.color;
				return col*0.6f;		
			}
			ENDCG
		}

		Pass
		{
			Cull Back
			Tags{ "LightMode" = "ForwardBase" }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
							// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"
			#include "UnityLightingCommon.cginc" // for _LightColor0

			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal: NORMAL;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvmask : TEXCOORD1;
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
				float4 diff: COLOR1;
			};

			sampler2D _MainTex;
			sampler2D _CutoutMask;
			float4 _MainTex_ST;
			float4 _CutoutMask_ST;
			float _CutoutThreshold;
			float4 _TintColor;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				o.uvmask = TRANSFORM_TEX(v.uv, _CutoutMask);
				o.color = v.color;
				o.diff = nl * _LightColor0;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 cut = tex2D(_CutoutMask, i.uvmask);
				clip(i.color.a - cut.r);

				col = col *i.color;
				col = col* (i.diff*0.5f + 0.5f);
				return col;
			}
			ENDCG
		}
	}
}
