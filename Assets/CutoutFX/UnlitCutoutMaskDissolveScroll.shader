﻿Shader "Maikel/FX/CutoutMaskScroll"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_CutoutMask("Cutout Mask", 2D) = "white" {}
		_CutoutThreshold ("Cutout Threshold", Range(0,1)) = 0
		_ScrollXSpeed ("Scroll X Speed", Range(0,100)) = 0
		_BorderThreshold("Border thickness", Range(0,1)) = 0
		_BorderColor("Border color", Color) = (1,1,1,1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100
		Cull Off
		Pass
		{
			

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
							// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color: COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uvmask : TEXCOORD1;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 color: COLOR;
			};

			sampler2D _MainTex;
			sampler2D _CutoutMask;
			float4 _MainTex_ST;
			float4 _CutoutMask_ST;
			float _CutoutThreshold;
			float _ScrollXSpeed;
			float _BorderThreshold;
			float4 _BorderColor;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				v.uv.x += _Time * _ScrollXSpeed;
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uvmask = TRANSFORM_TEX(v.uv, _CutoutMask);
				o.color = v.color;
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 cut = tex2D(_CutoutMask, i.uvmask);

				if (cut.g - _CutoutThreshold < _BorderThreshold)
				{
					col = _BorderColor;
				}

				clip(cut.g - _CutoutThreshold);
				return col;
			}
			ENDCG
		}
	}
}
