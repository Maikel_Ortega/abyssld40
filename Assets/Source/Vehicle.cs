﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : GameEntity 
{
	public float mass = 1f;
	public float maxSpeed = 10f;
	public float maxForce = 5f;
	public Vector3 currentVelocity;

	public virtual void Move(Vector3 movement)
	{
		if (movement.magnitude > 0.001f) 
		{
			transform.forward = movement.normalized;

			transform.Translate(movement,Space.World);

		}
	}
}
