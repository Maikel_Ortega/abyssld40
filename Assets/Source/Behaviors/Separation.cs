﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates a steering force away of every Autonomous Agent within a small distance
/// </summary>
public class Separation : SteeringBehavior 
{
    public Vector3 lastForce;
    public float forceMagnitude;

    public override void Init (AutonomousAgent agent)
	{
        agent.requiresNeighbours = true;
	}

	public override Vector3 Evaluate (AutonomousAgent agent)
	{
        Vector3 desiredVelocity = Vector3.zero;
        Vector3 toAgent= Vector3.zero;
        foreach (var curNeighbour in agent.neighbours)
        {
            toAgent = agent.transform.position - curNeighbour.transform.position;
            desiredVelocity += toAgent.normalized / toAgent.magnitude;
        }

        desiredVelocity*= agent.maxSpeed;

        lastForce = desiredVelocity;
        forceMagnitude = desiredVelocity.magnitude;

		return desiredVelocity;
	}

	void OnDrawGizmos()
	{
		if(drawGizmos)
		{
			Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position, transform.position + lastForce);
        }
    }

	public override string ToString ()
	{
		return string.Format ("[Separation]");
	}
}
