﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pursuit : SteeringBehavior 
{
	
    public AutonomousAgent pursuitTarget;
    public Color gizmoColor = Color.red;
    AutonomousAgent mAgent;

	public override void Init (AutonomousAgent agent)
	{
        this.mAgent = agent;
	}

	public override Vector3 Evaluate (AutonomousAgent agent)
	{
        return PursuitFunction(agent, pursuitTarget);
    }

    public static Vector3 PursuitFunction(AutonomousAgent hunter, AutonomousAgent prey)
    {
        Vector3 toTarget = prey.transform.position - hunter.transform.position;
        float targetSpeed = prey.currentVelocity.magnitude;
        float lookAheadTime = toTarget.magnitude / (hunter.maxSpeed + targetSpeed);

        Vector3 steeringForce = Seek.SeekFunction(hunter, prey.transform.position + prey.currentVelocity * lookAheadTime);
        return steeringForce;
    }


	void OnDrawGizmos()
	{
		if(drawGizmos)
		{
            Gizmos.color = gizmoColor;
            if (Application.isPlaying)
            { 
                Vector3 steering = PursuitFunction(mAgent, pursuitTarget);
                Gizmos.DrawLine(transform.position, transform.position + steering);
            }
        }
	}

	public override string ToString ()
	{
		return string.Format ("[Pursuit]");
	}
}
