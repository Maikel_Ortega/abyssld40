﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates a steering force towards the center of mass of the neighbour group.
/// </summary>
public class Cohesion : SteeringBehavior
{
    public Vector3 lastForce;
    public float forceMagnitude;

    public override void Init(AutonomousAgent agent)
    {
        agent.requiresNeighbours = true;
    }

    public override Vector3 Evaluate(AutonomousAgent agent)
    {
        Vector3 centerOfMass = Vector3.zero;
        Vector3 desiredVelocity = Vector3.zero;
        foreach (var curNeighbour in agent.neighbours)
        {
            centerOfMass += curNeighbour.transform.position;
        }

        if (agent.neighbours.Count > 0)
        {
            centerOfMass /= agent.neighbours.Count;

            desiredVelocity = (centerOfMass - agent.transform.position).normalized * agent.maxSpeed;
            desiredVelocity -= agent.currentVelocity;
        }

        lastForce = desiredVelocity;
        forceMagnitude = lastForce.magnitude;

        return desiredVelocity;
    }

    void OnDrawGizmos()
    {
        if (drawGizmos)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, transform.position + lastForce);

        }
    }

    public override string ToString()
    {
        return string.Format("[Cohesion]");
    }
}
