﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Behavior that creates a steering force away from any non trigger Collider.
/// </summary>
public class ObstacleAvoidance : SteeringBehavior 
{
	public Vector3 boxSize = Vector3.one;
	public float boundingRadius = 0.5f;
	public bool colliding = false;
	public Vector3 lastColPoint;
	public Vector3 boxCastOrigin;
	public Vector3 lastSteeringForce;

	public override void Init (AutonomousAgent agent)
	{
		
	}
	public override Vector3 Evaluate (AutonomousAgent agent)
	{
		RaycastHit info = new RaycastHit();
		Vector3 localSteeringForce = Vector3.zero;
		boxCastOrigin = agent.transform.position + agent.transform.forward * (boundingRadius -boxSize.z/2f - 0.5f);
		RaycastHit[] infos = Physics.BoxCastAll (boxCastOrigin,
			boxSize/2f,
			agent.transform.forward,
			agent.transform.rotation,
			boxSize.z);

		if (infos != null && infos.Length > 0)
			{
			info = infos [0];
			float closestDistance = Vector3.Distance( info.point, agent.transform.position);
			if (infos.Length > 1) {

				for (int i = 1; i < infos.Length; i++) 
				{
					var item = infos [i];
					float d = Vector3.Distance (item.point, agent.transform.position);	
					if (d < closestDistance) 
					{
						closestDistance = d;
						info = item;
					}
				}
			}

			Vector3 p = info.point;
			lastColPoint = p;
			if (!info.collider.isTrigger) 
			{
				colliding = true;
				float dist = (p - info.collider.transform.position).magnitude;
				Vector3 localP = agent.transform.InverseTransformPoint (p);
				Vector3 localObstacle = agent.transform.InverseTransformPoint (info.collider.transform.position);
			
				localSteeringForce.x = (-this.boundingRadius / localObstacle.x) * (0.2f*boxSize.z / dist);
				localSteeringForce = agent.transform.TransformVector (localSteeringForce);
				//localSteeringForce = -localP * dist;


			} else 
			{
				colliding = false;
				localSteeringForce = Vector3.zero;
			}
		} else 
		{
			colliding = false;
			localSteeringForce = Vector3.zero;
		}

		lastSteeringForce = localSteeringForce;
		return lastSteeringForce;
	}	

	void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		if (colliding) 
		{
			Gizmos.color = Color.red;
		}
		Gizmos.DrawSphere (boxCastOrigin, 0.25f);
		Gizmos.DrawSphere (lastColPoint, 0.1f);
		Gizmos.DrawLine(transform.position, transform.position + lastSteeringForce);

		Gizmos.matrix = Matrix4x4.TRS (transform.position, transform.rotation, transform.localScale);
		Gizmos.DrawWireCube (Vector3.forward *(boundingRadius+  (boxSize.z / 2f)),boxSize);
	}

	public override string ToString ()
	{
		return string.Format ("[ObsAvoid.]");
	}
}
