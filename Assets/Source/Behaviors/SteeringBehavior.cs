﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class SteeringBehavior: MonoBehaviour 
{
	public bool drawGizmos;
	public abstract void Init(AutonomousAgent agent);
	public abstract Vector3 Evaluate(AutonomousAgent agent);
}
