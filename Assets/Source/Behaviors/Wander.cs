﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : SteeringBehavior 
{
	public float wanderRadius;
	public float wanderDistance;
	public float wanderJitter;
	public Vector3 wanderTarget;
	private Vector3 lastTargetPos;


	public override void Init (AutonomousAgent agent)
	{
		wanderTarget = Vector3.zero;
		lastTargetPos = wanderTarget;
	}

	public override Vector3 Evaluate (AutonomousAgent agent)
	{
		//Creamos un desplazamiento aleatorio
		Vector3 displacement = new Vector3(Random.Range(-1f,1f)*wanderJitter, Random.Range(-1f,1f)*wanderJitter, Random.Range(-1f,1f)*wanderJitter); 
		//Lo sumamos a la ultima posición
		wanderTarget += displacement;
		//Normalizamos a una esfera de radio 1
		wanderTarget = wanderTarget.normalized;
		//Pasamos a una esfera del radio especificado
		wanderTarget *= wanderRadius;
		//Proyectamos esa esfera delante nuestra en coordenadas de mundo
		lastTargetPos = (agent.transform.position + agent.transform.forward * wanderDistance) + wanderTarget;
		//Devolvemos el vector que lleva a nuestro punto.
		return  (lastTargetPos - agent.transform.position).normalized*agent.maxSpeed;
	}

	void OnDrawGizmos()
	{
		if(drawGizmos)
		{
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(lastTargetPos,0.25f);
			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(transform.position + transform.forward*wanderDistance,wanderRadius);
		}
	}

	public override string ToString ()
	{
		return string.Format ("[Wander]");
	}
}
