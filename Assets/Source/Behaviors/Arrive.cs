﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrive : SteeringBehavior 
{
	public Vector3 target;
    public float slowingDistance=1f;
    public Color stoppingRadiusColor;
    AutonomousAgent mAgent;

	public override void Init (AutonomousAgent agent)
	{
        this.mAgent = agent;
	}

	public override Vector3 Evaluate (AutonomousAgent agent)
	{
        Vector3 steeringForce = ArriveFunction(agent.transform.position, this.target, this.slowingDistance, agent.maxSpeed);
        steeringForce = steeringForce - agent.currentVelocity;
        return steeringForce;
    }

    /// <summary>
    /// Returns the desired velocity to arrive at a certain target, given the agent, said target, a slowing distance to start braking, and the maximum speed of the agent.
    /// </summary>
    /// <param name="agentPosition"></param>
    /// <param name="target"></param>
    /// <param name="slowingDistance"></param>
    /// <param name="agentMaxSpeed"></param>
    /// <returns></returns>
    public static Vector3 ArriveFunction(Vector3 agentPosition, Vector3 target, float slowingDistance, float agentMaxSpeed)
    {
        Vector3 toTarget = (target - agentPosition);
        float dist = toTarget.magnitude;        
        float rampedSpeed = agentMaxSpeed * (dist / slowingDistance);
        float clippedSpeed = Mathf.Min(rampedSpeed, agentMaxSpeed);
        Vector3 desiredVelocity = (clippedSpeed / dist) * toTarget;
        return desiredVelocity;        
    }

	void OnDrawGizmos()
	{
		if(drawGizmos)
		{
			Gizmos.color = Color.magenta;
			Gizmos.DrawWireSphere(target,0.25f);
            Gizmos.color = stoppingRadiusColor;
            Gizmos.DrawSphere(target, slowingDistance);
            if (Application.isEditor && Application.isPlaying && mAgent != null)
            { 
                Vector3 steeringForce = ArriveFunction(mAgent.transform.position,  target,  slowingDistance, mAgent.maxSpeed) - mAgent.currentVelocity;
                Gizmos.DrawLine(transform.position, transform.position +steeringForce);
            }
		}
	}

	public override string ToString ()
	{
		return string.Format ("[Arrive]");
	}
}
