﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seek : SteeringBehavior 
{
	public Vector3 target;

	public override void Init (AutonomousAgent agent)
	{
	}

	public override Vector3 Evaluate (AutonomousAgent agent)
	{
        return SeekFunction(agent, target);
	}

    /// <summary>
    /// Returns the steeringForce to Seek a given target
    /// </summary>
    /// <param name="agent"></param>
    /// <param name="seekTarget"></param>
    /// <returns></returns>
    public static Vector3 SeekFunction(AutonomousAgent agent, Vector3 seekTarget)
    {
        Vector3 desiredVelocity = (seekTarget - agent.transform.position).normalized * agent.maxSpeed;
        return desiredVelocity - agent.currentVelocity;
    }

	void OnDrawGizmos()
	{
		if(drawGizmos)
		{
			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(target,0.25f);
		}
	}

	public override string ToString ()
	{
		return string.Format ("[Seek]");
	}
}
