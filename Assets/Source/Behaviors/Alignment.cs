﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates a steering force away of every Autonomous Agent within a small distance
/// </summary>
public class Alignment : SteeringBehavior 
{
    public Vector3 lastForce;
    public float forceMagnitude;

	public override void Init (AutonomousAgent agent)
	{
        agent.requiresNeighbours = true;
	}

	public override Vector3 Evaluate (AutonomousAgent agent)
	{
        Vector3 avgHeading = Vector3.zero;

        foreach (var curNeighbour in agent.neighbours)
        {
            avgHeading += curNeighbour.transform.forward; 
        }

        if (agent.neighbours.Count > 0)
        {
            avgHeading /= agent.neighbours.Count;
            avgHeading -= agent.transform.forward;
            avgHeading = avgHeading.normalized *agent.maxSpeed;
        }
        lastForce = avgHeading;
        forceMagnitude = avgHeading.magnitude;
		return lastForce;
	}

	void OnDrawGizmos()
	{
		if(drawGizmos)
		{
			Gizmos.color = Color.magenta;
            Gizmos.DrawLine(transform.position, transform.position + lastForce);
		}
	}

	public override string ToString ()
	{
		return string.Format ("[Alignment]");
	}
}
