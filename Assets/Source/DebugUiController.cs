﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUiController : MonoBehaviour 
{
	public Slider[] sliders;

	public void SetSteerForce(int index, float value, string name)
	{
		if(index <   sliders.Length)
		{
			Slider s = this.sliders[index];
			s.value = value;
			s.GetComponentInChildren<Text>().text = string.Format("{0}  {1}%",name, ((int)(value*100)).ToString());
		}
	}
}
