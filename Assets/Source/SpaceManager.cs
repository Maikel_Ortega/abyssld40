﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceManager : MonoBehaviour 
{
	public static SpaceManager Instance;
	public int cellSize = 10;
	public int cellRows = 10;

	[Header("Gizmos")]
	public Color gridColor = Color.blue;

	void Awake () 
	{
		SpaceManager.Instance = this;	
	}

	void OnDrawGizmos()
	{
		Gizmos.color = gridColor;
		Vector3 p1;
		Vector3 p2;
		Vector3 p3;
		Vector3 p4;
		Vector3 p5;
		Vector3 p6;

		for (int i = 0; i < cellRows; i++) 
		{
			for (int j = 0; j < cellRows; j++) 
			{
				for (int k = 0; k < cellRows; k++) 
				{
					Gizmos.DrawWireCube (new Vector3 (i+cellSize/2, j+cellSize/2, k+cellSize/2), Vector3.one* cellSize);
				}
			}	
		}
	}
}
