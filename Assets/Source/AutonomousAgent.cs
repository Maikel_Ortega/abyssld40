﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SteeringConfiguration
{
	public SteeringBehavior behavior;
	public float weight;
}

public class AutonomousAgent : Vehicle 
{
	public List<SteeringConfiguration> behaviorConfigsByPriority;

    [Header("Neighbours")]
    public bool requiresNeighbours = false;
    public float neighbourRadius = 10f;
    public string neighbourTag = "steering";
    public List<AutonomousAgent> neighbours;

	[Header("Debug")]
	public bool drawGizmos;
	public DebugUiController debugUI;
    public Color nextPosColor;

	void Start()
	{
        GlobalAgentsManager.Instance.Subscribe(this);


		foreach (var item in behaviorConfigsByPriority) 
		{
			item.behavior.Init(this);	
		}
	}

	protected virtual void Update()
	{
        if (requiresNeighbours)
        { 
            this.neighbours = GlobalAgentsManager.Instance.GetNeighbours(this, neighbourRadius, neighbourTag);
        }

        Vector3 steeringForce = GetSteeringForce();
		Vector3 acceleration = steeringForce/mass;
		this.currentVelocity += acceleration *Time.deltaTime;
		this.currentVelocity = Vector3.ClampMagnitude(this.currentVelocity,maxSpeed);
		this.Move(currentVelocity*Time.deltaTime);
	}

	Vector3 GetSteeringForce()
	{
		Vector3 currentSteeringForce = Vector3.zero;
		float forceRemaining = maxForce;

        float totalWeights = 0;

		for(int i = 0; i < behaviorConfigsByPriority.Count; i++)
		{
			if(forceRemaining > 0)
			{
				Vector3 force = behaviorConfigsByPriority[i].behavior.Evaluate(this);

                float w = behaviorConfigsByPriority[i].weight;
                totalWeights += w;
                force *= w;

				Vector3 clampedForce = Vector3.ClampMagnitude(force,forceRemaining);

				float assignedForce = clampedForce.magnitude;

				forceRemaining-= assignedForce;
				currentSteeringForce += clampedForce;
				Debug.Log(behaviorConfigsByPriority[i].behavior.ToString()+ " Added a force of "+ assignedForce.ToString());

				if(debugUI != null)
				{
					debugUI.SetSteerForce(i,assignedForce/maxForce,behaviorConfigsByPriority[i].behavior.ToString());
				}
			}
			else
			{
				if(debugUI != null)
					debugUI.SetSteerForce(i,0f,behaviorConfigsByPriority[i].behavior.ToString());
				return currentSteeringForce;
			}
		}

        currentSteeringForce /= totalWeights;

		return currentSteeringForce;
	}

	void OnDrawGizmos()
	{
		if(this.drawGizmos)
		{
			Gizmos.color = Color.cyan;
			Gizmos.DrawRay(transform.position, currentVelocity);
            Gizmos.color = nextPosColor;
            Gizmos.DrawCube(transform.position + currentVelocity*Time.deltaTime,Vector3.one);
		}
	}
}
