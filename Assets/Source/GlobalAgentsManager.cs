﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Singleton class responsable of keeping the reference to every Autonomous Agent
/// </summary>
public class GlobalAgentsManager : MonoBehaviour
{
    public static GlobalAgentsManager Instance;

    public List<AutonomousAgent> agents;

	void Awake ()
    {
        GlobalAgentsManager.Instance = this;
	}

    public void Subscribe(AutonomousAgent agent)
    {
        if (agents == null)
        {
            agents = new List<AutonomousAgent>();
        }

        agents.Add(agent);
    }

    public List<AutonomousAgent> GetNeighbours(AutonomousAgent me, float radius, string neighbourTag)
    {
        List<AutonomousAgent> neighbours = new List<AutonomousAgent>();
        foreach (var item in agents)
        {
            if (item != me && item.neighbourTag == neighbourTag && IsInsideRadius(me, item, radius))
            {
                neighbours.Add(item);
            }
        }

        return neighbours;
    }

    /// <summary>
    /// Returns true if B is within a certain radius of A
    /// </summary>
    /// <param name="A"></param>
    /// <param name="B"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    public bool IsInsideRadius(AutonomousAgent A, AutonomousAgent B, float radius)
    {
        return (Vector3.Distance(A.transform.position, B.transform.position)<= radius);
    }
}
